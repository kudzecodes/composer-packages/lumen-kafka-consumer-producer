<?php

namespace Kudze\LumenKafkaConsumerProducer\Command;

use RdKafka\Message;

abstract class AbstractConsumerJsonCommand extends AbstractConsumerCommand
{
    protected abstract function processJsonMessage(Message $message, ?array $payload): void;

    protected function processMessage(Message $message): void
    {
        $this->processJsonMessage($message, json_decode($message->payload, true));
    }
}