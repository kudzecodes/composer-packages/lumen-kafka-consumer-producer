<?php

namespace Kudze\LumenKafkaConsumerProducer\Command;

use Kudze\KafkaConsumerProducer\Services\KafkaConsumer;
use Kudze\LumenBaseCli\Command\AbstractCommand;
use RdKafka\Exception as KException;
use RdKafka\Message;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractConsumerCommand extends AbstractCommand
{
    protected KafkaConsumer $consumer;

    protected abstract function getKafkaTopics(): array;

    protected abstract function processMessage(Message $message): void;

    public function __construct(KafkaConsumer $consumer)
    {
        $this->consumer = $consumer;

        parent::__construct();
    }

    /**
     * @throws KException
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);

        $this->consumer->subscribe($this->getKafkaTopics());
    }

    /**
     * @throws KException
     */
    public function handle(): int
    {
        parent::handle();

        $this->consume();

        $this->io->info("Done!");
        return self::SUCCESS;
    }

    /**
     * @throws KException
     */
    protected function consume(): void
    {
        while (true) {
            $message = $this->consumer->consume();

            if ($message->err === RD_KAFKA_RESP_ERR__TIMED_OUT) {
                if (!$this->onTimeout()) break;
                continue;
            }

            if ($message->payload === null) {
                $this->onEmptyPayload();
                continue;
            }

            $offset = $message->offset;
            $this->io->writeln("Consumed $offset...");
            $this->processMessage($message);
        }
    }

    /**
     * returns true if consumer needs to continue, false if it needs to stop.
     *
     * @return bool
     */
    protected function onTimeout(): bool
    {
        $this->io->info("Timed out error, skipping");
        return true;
    }

    protected function onEmptyPayload(): void
    {
        $this->io->warning("Message had null payload, skipping...");
    }
}